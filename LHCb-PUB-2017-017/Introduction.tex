\section{Introduction}
\label{sec:intro}



The pitch between strips of silicon microstrip sensors and the pitch between input channels of the application-specific integrated circuits (ASICs) used for readout can often differ by large amounts. One solution is to use external adapters such as metal traces on a glass substrate, but this presents its own difficulties for the construction of large detectors.  While the required wire bond geometry is consistent with industry standards, the number of bonds needed doubles. Moreover, additional space and material is introduced, which may be difficult to accommodate in densely packed silicon detectors.

Another solution to this problem is to use embedded pitch adapters built into the silicon sensors themselves.  During sensor fabrication a second metal layer of traces with bond pads designed to match the readout ASIC pitch is placed on top of the silicon dioxide passivation.  The AC-coupled metal strips of the first layer are directly connected to this second layer above. 

This technology has been studied for the silicon tracker upgrade for the ATLAS experiment~\cite{Ullan2013178} and the Upstream Tracker (UT) upgrade for the LHCb experiment~\cite{LHCb-TDR-015}.
In the studies of the p-substrate sensor prototypes designed for the ATLAS experiment it was shown that the pitch adapters have an effect on the measured capacitance between strips~\cite{Ullan2016221}. However, no evidence of ``pick-up'' (creating spurious signals on far away strips) or of signal loss on nearby strips was found.

During research and development for the UT, we observed a loss of efficiency localized in the pitch adapter (PA) region in both n- and p-substrate sensors.  Maps of the locations of passing charged particles that did not leave a reconstructed signal in the prototype sensor are shown in \cref{fig:xray}.  The inefficiency maps out the location of the embedded pitch adapter on these sensors.

\begin{figure}[htb]
\begin{subfigure}{0.5\textwidth}
 \twgraphic{xray_a3_s1}
  \caption{n-substrate sensor\label{fig:xray_ntype}}
\end{subfigure}
\begin{subfigure}{0.5\textwidth}
\twgraphic{xray_13_hm2_s1}
\caption{p-substrate sensor\label{fig:xray_ptype}}
\end{subfigure}
\caption{ Positions on two sensors of charged particles for which a corresponding signal is not found in the sensor.  The inefficient band at the top corresponds to the edge of the sensor active area.  The pattern of inefficiency is confined to the pitch adapter region of the sensor and only in the region between two strips.  The n-type sensor (\protect\subref{fig:xray_ntype}) shows a small inefficiency near certain bond pads, while the p-type sensor (\protect\subref{fig:xray_ptype}) shows a much stronger effect.  The vertical bands with no missed hits are regions removed from analysis due to strips flagged as bad. \label{fig:xray}}
\end{figure}

In this paper, we focus on the effect in n-substrate sensors, using prototypes designed for the LHCb upgrade.  We have simulated the effects of the PA on signal formation in the sensor to understand the cause of the observed effects.  We then confirm further predictions of the simulation regarding the details of the pick-up effects.  

\subsection{Sensor description}

The sensors used are miniature ($\SI{1.8}{\centi\meter}\times\SI{1.4}{\centi\meter}$) silicon microstrip sensors produced by Hamamatsu Photonics~\cite{hamamatsu}.  The silicon is \SI{320}{\micro\meter} thick and of p$^+$-in-n type.  Each sensor has 64 strips with a pitch of \SI{190}{\micro\meter}.  The sensor is designed to be AC coupled to the readout ASICs with a pitch of approximately \SI{80}{\micro\meter} through the use of an embedded pitch adapter.

The design of the pitch adapter is pictured in \cref{fig:sensor}. Metal traces with a width of approximately \SI{10}{\micro\meter} connect each channel to a bonding pad \SI{65}{\micro\meter} wide (perpendicular to the strips) and \SI{210}{\micro\meter} long (parallel to the strips).  The distance between the metal traces in the densest region varies from \SIrange{10}{20}{\micro\meter}.   These traces and bonding pads are located on top of the active sensor area.  The two metal layers of the pitch adapter and the coupling capacitor strips are separated by a layer of silicon dioxide that is \SI{2}{\micro\meter} thick.  The sensor's metal strips are \SI{123}{\micro\meter} wide, with a gap between of \SI{67}{\micro\meter}.

\begin{figure}[htb]
  \includegraphics[width=\textwidth,angle=180]{pitch_adapter_layout}
  \caption{Zoomed in view of the sensor metallization layers, showing one-half of the pitch adapter.  The hatched purple vertical bands show the metal strips above each silicon strip.  Thirty-two channels are connected via the second-metal layer shown in solid red, to the closely spaced bond pads in the center.  The other thirty-two channels are similarly connected with a mirror-image design not pictured. \label{fig:sensor}}
\end{figure}

The analysis in this paper focuses on the bond pad region, where the larger metal area is expected to cause stronger effects.  The bond pads almost completely bridge the interstrip region when they are centered relative to the strips below.  In other cases, the bond pad is shifted to one side or the other, partially or fully overlapping the metal strip.
