\section{Results from beam test}
\label{sec:nresults}

Two dimensional views of the sampled ADC versus time are shown in \cref{fig:npulse2d} for the PA and control regions.  The control region shows a single clear peak in the signal pulse as a function of time.  Two qualitatively different signal patterns are clear in the data from the PA region.  In some events, there is a clear signal peak on the two closest strips similar to the control region, while in others the nearby strips show a value close to zero.  In the latter case, the resulting data would often fail clustering requirements to separate signal from noise and the hit would be missed.  These two classes of events are separated by requiring the sum ADC on the two nearby strips to be less than or greater than 150.  Since the signal is split almost evenly between the two strips, this is approximately equivalent to a cut of three times the noise applied to each strip separately.

\begin{figure}[htb]
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{ntyperesults/ctl_sum_btwn}
    \caption{Control region\label{fig:npulse2d_ctl}}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{ntyperesults/pa_sum_btwn}
    \caption{PA region\label{fig:npulse2d_pa}}
  \end{subfigure}
  \caption{Sum of the ADC values for the two strips near the track location versus time for an unirradiated sensor.  The tracks in the control region are shown in (\protect\subref{fig:npulse2d_ctl}) and in the PA region in (\protect\subref{fig:npulse2d_pa}). \label{fig:npulse2d}}
\end{figure}

To reconstruct the pulse shape for each class of events, the peak ADC value is first determined separately for each time bin.  The pulse is reconstructed separately for nearby strips and the PA connected strip.  For the nearby strips, the ADC values of the two channels are added together before finding the peak.  Three different methods for peak finding are used:
\begin{itemize}
\item For good signals ($\text{ADC} > 150$) on the nearby strips, the ADC distribution is fitted with a Landau convolved with a Gaussian.  The peak position is taken from the most probable value.
\item False ``signals'' on the PA connected strip show a different ADC distribution without the characteristic high-side tail of a Landau distribution, but this may be due to the small number of recorded events. For these outputs, the mean value from a Gaussian fit is used to find the peak position.
\item For channels without clear signals (cross-talk and inefficient hits), the mean of the ADC values is taken as the peak position.
\end{itemize}

Once the peak value in each bin has been determined, the resulting points are fit with a quadratic function to qualitatively describe the shape of the pulse within the \SI{25}{\nano\second} window.  The results are shown in \cref{fig:npulse}.  The two cases of a ``good'' signal  ($\text{ADC} > 150$) found or not found on the nearby strips are compared.  In the pitch adapter region, good signals are slowed in time and reduced in magnitude relative to the control region pulse; the peak occurs approximately \SI{5}{\nano\second} later.  Simultaneously, a large cross-talk like signal appears on the PA connected strip, already decreasing in the sampled time window.  These results match qualitatively the expectation from simulation.


\begin{figure}[htb]
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{ntyperesults/pa_pulse}
    \caption{\label{fig:npulse_good} With ``good'' signal }
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{ntyperesults/far_pulse}
    \caption{\label{fig:npulse_bad} No ``good'' signal }
  \end{subfigure}
  \caption{Reconstructed pulse shapes within the \SI{25}{\nano\second} readout window for particles passing halfway between two strips.  The pulses in the control and PA regions represent the sum of the output on the two nearby channels.  In (\protect\subref{fig:npulse_good}), a good signal is found on the nearby strips when the particle lands in the PA region; in (\protect\subref{fig:npulse_bad}) a good signal is not found.
    \label{fig:npulse}}
\end{figure}

When the signal on the nearby strips is lost, a pulse similar in shape to a good signal is instead found on the PA connected strip.  The nearby strips show only a small signal above zero.  The source of these events can be determined by further subdividing the data sample.  For two of the 14 strip pairs, the PA bond pad is centered above the interstrip region.  For six pairs it is shifted by approximately \SI{10}{\micro\meter} towards one side, and the other six towards the other side.  In \cref{fig:ntype_dx_miss}, the fraction of missing hits for each of these three groupings is shown as a function of the interstrip position normalized by the strip pitch ($\Delta x /P$).  For the centered bond pads, no inefficiency is found.  The shifted bond pads show an inefficiency in the opposite side of the interstrip region which is consistent with the expectation from simulation.  The local inefficiency reaches approximately 50\% at the highest.  The overall effect averaged over the sensor is small since the region of inefficiency is limited to this small area.

\begin{figure}[htb]
  \begin{center}
  \includegraphics[width=0.5\textwidth]{ntyperesults/dx_miss}
  \end{center}
  \caption{Fraction of particles with missing signals on the nearby channels as a function of the interstrip position for particles passing through the PA region.  $\Delta x/P = 0.5$ corresponds to halfway between the two strips.  Three groups of channels are shown, corresponding to cases in which the PA bond pad is either centered between the two strips, or shifted towards one side or the other.
    \label{fig:ntype_dx_miss}}

\end{figure}

We find that the effects of the PA are reduced with irradiation, as shown in \cref{fig:npulse_irrad}.  By a dose of \SI{6.4}{\kilo\gray} in the silicon dioxide layer, no PA coupling effects are found.  At lower levels of irradiation, the effects on the nearby strip signals and on the PA connected channel are reduced but not totally eliminated.  With the measurements available on these sensors, we are unable to determine precisely the level of charge build-up in the oxide layer as a function of dose.


\begin{figure}[htb]
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{ntyperesults/pa_pulse_irrad}
    \caption{Near channels\label{fig:npulse_near_irrad}}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{ntyperesults/pa_far_irrad}
    \caption{PA connected channel\label{fig:npulse_far_irrad}}
  \end{subfigure}
  \caption{Pulse shapes for three sensors with different radiation doses in the silicon dioxide layer.  As irradiation increases, the PA coupling effect is reduced, both in the delay in the signal observed in the nearest two channels (\protect\subref{fig:npulse_near_irrad}) and in the PA connected channel signal (\protect\subref{fig:npulse_far_irrad}). \label{fig:npulse_irrad}}
\end{figure}
