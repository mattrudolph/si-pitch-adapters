TARGETS=main.pdf

all: $(TARGETS)

%.pdf: %.tex *.tex
	pdflatex $<
	bibtex $(basename $<)
	pdflatex $<
	pdflatex $<

clean:
	rm -f $(TARGETS) main_lhcb.pdf *.aux *.toc *.log *.bbl *.blg *.out *.run.xml *.bcf

over: clean all

lhcb: main_lhcb.pdf

