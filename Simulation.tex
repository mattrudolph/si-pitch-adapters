\section{Simulation}
\label{sec:simulation}

We simulate the operation of the sensor in the pitch adapter region using the TCAD Sentaurus package, part of Synopsys software~\cite{Synopsys}. We use a two dimensional simulation of the full sensor in a slice perpendicular to the strips, including two implant strips and a metal strip to model the pitch adapter bond pad.  The geometry of the model is shown in \cref{fig:scheme_Interstrip}.

\subsection{Model description}

The exact parameters of the sensors manufactured by Hamamatsu are not known, therefore the choice of many parameters for the simulation was inspired by studies of similar silicon sensors produced by Hamamatsu for CMS~\cite{EberThesis}. We assume the bulk to be doped with phosphorus (n-type) with a concentration of \SI{3e12}{\per\cubic\centi\meter}.  The strips implants are doped with boron (p$^+$) with a much higher concentration of \SI{1e19}{\per\cubic\centi\meter}.  A Gaussian error function is used for the implant doping profile with a depth of \SI{1}{\micro\meter} and a width of \SI{1}{\micro\meter}.  Variations of the implant profile 
depth and width in ranges 1-\SI{5}{\micro\meter} and 0.5-\SI{1.5}{\micro\meter}, respectively,
have shown no significant effects on the results of the simulation.
The bottom side of the bulk is doped with phosphorus (n$^+$) with a concentration of \SI{0.5e19}{\per\cubic\centi\meter} to achieve ohmic contact with the metal backplane. A density of positive charges trapped at the boundary of the silicon and silicon dioxide of \SI{1e11}{\per\square\centi\meter} is assumed for unirradiated sensors. To emulate the effect of irradiation, the charge density is increased to its saturation value of \SI{3e12}{\per\square\centi\meter}.  These numbers are based on typical values for silicon--silicon dioxide boundaries~\cite{mos,JSCHWANDT2017159}, but have not been measured on the tested sensors.  It is possible that the tested sensors start with a lower charge density than \SI{1e11}{\per\square\centi\meter} when unirradiated, but changes to the qualitative behavior of the sensors is not observed until higher values.

We chose for the width of the simulation region to be twice the pitch of \SI{190}{\micro\meter}, and thus it includes two p$^+$ strips each \SI{120}{\micro\meter} wide. The p$^+$ implants are connected to electrical ground via \SI{1.5}{\mega\ohm} resistors and AC coupled to the metal strips placed at \SI{300}{\nano\meter} above them in the silicon dioxide layer. When simulating the pitch adapter pads, a second metal layer on top of the silicon dioxide is held at electrical ground to represent the  \SI{65}{\micro\meter} wide pitch adapter pads.  Three pads are included with a separation of \SI{10}{\micro\meter}; the center pad is  placed over the inter-strip region; its edge is at a distance of \SI{2.5}{\micro\meter} from the edge of the strip. A positive high voltage bias of \SI{300}{\volt} (the depletion voltage is approximately \SI{250}{\volt}) is applied to the backside contact. An adaptive meshing with cell dimensions from $\SI{20}{\micro\meter}\times\SI{50}{\micro\meter}$ to $\SI{1}{\micro\meter}\times\SI{1}{\micro\meter}$ is used.

\begin{figure}[htb]
\begin{center}
  \includegraphics[width=0.5\textwidth]{simulation/scheme_Interstrip}
\end{center}
  \caption {Two dimensional model of the sensor used for the simulation.  The successive insets zoom in on the details of the
    implant and metalization geometry near the sensor
    surface. The metal layers of the strip AC-metal and pitch adapter have had their thickness exaggerated to improve the visualization.\label{fig:scheme_Interstrip}}
\end{figure}

It was shown in Ref.~\cite{EberThesis} that the TCAD Sentaurus default
values for the most important parameters (permittivity, electron
affinity, band gap energy, permittivity of silicon dioxide) are well
justified and therefore were used in this study. Though the default
generation and recombination times of electron and holes are lower
than those of very clean silicon, we do not expect it to have a
significant influence on the results of this investigation.  The
simulation of charge carrier mobility accounts for velocity saturation
at high fields and doping dependency.  The generation and
recombination of charge carriers is implemented according to the
Shockley-Read-Hall recombination model.  At the boundary an absence of
electric field and carrier current perpendicular to the boundary are
required~\cite{BoundaryPennicard}. %Effectively it is equal to mirror extension of the device.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Results of the simulation}

The electrostatic potential maps from simulation are shown in \cref{fig:INTmap} for configurations with and without the pitch adapter metal layer. The presence of the pitch adapter layer changes the electric field in the inter-strip region close to the silicon surface.  The charge carriers (holes) are attracted to the silicon-oxide surface and only then travel towards the strip implants. A shift in the PA pad position with respect to the middle of the inter-strip region results in a remarkable effect. An electrostatic potential at the silicon-oxide boundary rises near the strip opposite to the shift direction, producing a barrier as shown in \cref{fig:PotentialProfile}.  The gradient of the potential in the $x$ direction increases the coupling to the second metal and decreases the charge collection.

This potential creates a region of zero electric field as shown in \cref{fig:efield}.  The field prevents the charge carriers from reaching the nearby strip for a
timescale long compared to the usual readout.  This results in a signal loss on the strip and signal pick-up on the corresponding PA pad, where the induced charge is longer lived.
In case of saturated charge density in the silicon-oxide interface (corresponding to an irradiated sensor) the effect of the PA pads is suppressed and the resulting potential is the same as without PA pads.

\begin{figure}[tb]
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{simulation/INTmap_potential_noPA}
    \caption{No PA}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{simulation/INTmap_potential_withPA}
    \caption{With PA}
  \end{subfigure}
  \caption {Simulated map of the electrostatic potential in the inter-strip
    region at \SI{300}{\volt} bias. The left figure corresponds to a
    configuration without pitch adapter pads, while the right one
    includes them. The white line represents the boundary of the
    depletion region. The metal layers of the strip AC-metal and pitch adapter have had their thickness exaggerated to improve the visualization.\label{fig:INTmap}}
\end{figure}

\begin{figure}[tb]

  \begin{subfigure}{0.5\textwidth}
    \twgraphic{simulation/Potential_PAshift1}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{simulation/Potential_PAshift2}
  \end{subfigure}
 \caption {Simulated electrostatic potential profiles at the silicon--oxide
   boundary in the inter-strip region for various PA pad positions and
   without PA.  Zero PA shift centers the PA metal pad between two strips, \SI{2.5}{\micro\meter} from either strip; each shift increases the distance on one side. Right figure shows zoomed profiles. The dotted line
   with black circles represents the case without any PA pads. The
   dashed line with crosses represents the case of an irradiated sensor
   modeled by using saturation value of
   \SI{3e12}{\per\square\centi\meter} for oxide charge density
   (irrespective of presence and position of PA pads).
 \label{fig:PotentialProfile}}
\end{figure}

\begin{figure}[tb]
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{simulation/Ox2_PA2_Shift0_EField_zoom}
    \caption{Centered PA\label{fig:efield_cent}}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{simulation/Ox2_PA2_Shift10_EField_zoom}
    \caption{PA shifted \SI{10}{\micro\meter}\label{fig:efield_shift}}
  \end{subfigure}
  \caption{ Simulated electric field maps in the region near the silicon surface close to a strip implant.  The color scale represents the field strength and the printed arrows the direction.  In (\protect\subref{fig:efield_cent}), the PA is centered.  In (\protect\subref{fig:efield_shift}), the PA is shifted \SI{10}{\micro\meter}, creating a zero field region near the surface that blocks charge moving towards the strip on the left side.  The metal layers of the strip AC-metal and pitch adapter have had their thickness exaggerated to improve the visualization.\label{fig:efield}}
\end{figure}

To investigate signal pulse shapes, we simulate hits of minimum ionizing particles (MIP) penetrating the sensor perpendicularly to its plane, near to the halfway point between two strips. The MIP ionization density is tuned to produce approximately \SI{2e4} electron-hole pairs in the bulk.  The total charge induced on the strips' coupling capacitors and on the pitch adapter metal (before any shaping) as a function of the integration time are shown in \cref{fig:MIP} for two variants of PA position. The reference shape corresponds to the pulse from a particle passing directly under a strip. When the PA pads are centered with respect to the inter-strip region, slower collection on the nearby strips is observed with respect to the reference while a pick-up signal on the PA emerges with a peaking time of approximately \SI{5}{\nano\second} (\cref{fig:MIP_noshift}). In the case where the PA pads are shifted \SI{10}{\micro\meter}, resulting in a distance to the farther strip of \SI{12.5}{\micro\meter}, the pick-up signal on the PA becomes more similar to the reference while the signal on the strips is reduced significantly.  All the charge eventually reaches the strips on the order of microseconds.  This case is likely to result in a localized region of inefficiency.


\begin{figure}[tb]
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{simulation/MIP_noShift}
    \caption{Centered PA \label{fig:MIP_noshift}}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{simulation/MIP_Shift10}
    \caption{PA shifted \SI{10}{\micro\meter} \label{fig:MIP_shift}}
  \end{subfigure}
  \caption {Simulated charge appearing on the strip AC and PA contacts versus time since MIP hit in the inter-strip region \SI{5}{\micro\meter} closer to one of the strips. The reference curve corresponds to charge on the strip AC contact in response to a MIP passing under the strip.  To account for charge sharing, the sum of the charge collected on both strips is used.}
 \label{fig:MIP}
\end{figure}

In order to qualitatively compare with data from beam tests, a simple shaping function of the form:
\begin{equation}
  W( t ) = \frac{t}{\tau}e^{-t/\tau},
\end{equation}
has been applied to the charges, where $\tau$ is a shaping time of \SI{25}{\nano\second}.  The results are shown in \cref{fig:pulses}.  When the PA is centered, the slowed development of the signal on the nearby strips should be visible as a shift in the time of the pulse peak.  The pick-up signal on the PA resembles a very large cross-talk signal.  When the PA shifts, the signal on the nearby strips is greatly reduced.  The PA then sees a pulse similar in strength to a real signal.


\begin{figure}[tb]
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{simulation/pulse_noShift}
    \caption{Centered PA \label{fig:pulse_noshift}}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \twgraphic{simulation/pulse_Shift10}
    \caption{PA shifted \SI{10}{\micro\meter} \label{fig:pulse_shift}}
  \end{subfigure}
  \caption{Simple pulse shapes produced from the simulated charge appearing on the strip AC and PA contacts versus time since MIP hit in the inter-strip region \SI{5}{\micro\meter} closer to one of the strips. The reference curve corresponds to the one in \cref{fig:MIP}.  To account for charge sharing, the sum of the charge collected on both strips is used.  The vertical lines denote a \SI{25}{\nano\second} interval, motivated by the time window imposed by the data acquisition system on data from the beam test.}
 \label{fig:pulses}
\end{figure}

