\section{Beam test}
\label{sec:testbeam}

The sensors have been tested using beams delivered from the CERN SPS to the H8 beam line in the CERN North Area.  This beam delivers positively charged hadrons with a momentum of \SI{180}{\giga\evolt}.
  The width of the beam is approximately \SI{1}{\centi\meter}, allowing the beam to simultaneously illuminate the pitch adapter area and the edges of the sensor active area.

\subsection{Experimental setup}

The sensor, referred to as the device under test (DUT), is placed at the center of the Timepix3 telescope; an earlier iteration of this telescope is described in Ref.~\cite{Akiba:2013yxa}.  This telescope consists of 8 Timepix3 pixel modules used to reconstruct the trajectory of passing charged particles.  These tracks are extrapolated to where the DUT is placed with a pointing resolution of approximately \SI{2}{\micro\meter}.  This allows for a precise determination of the impact points of the beam particles with respect to the sensor structures like the strip implants and pitch adapter.  The beam impacts the DUT perpendicularly.

The sensors are read out using the Beetle ASIC~\cite{Lochner:2006vba}.  This chip outputs the signals on every channel as an analog pulse with a shaping time on the order of tens of \si{\nano\second}.  These signals are processed using the MAMBA data-acquisition (DAQ) board produced by Nuclear Instruments~\cite{mamba}.
This DAQ system samples and digitizes the analog pulse at a fixed time that can be moved in \SI{25}{\nano\second} increments.  The beam particles arrive asynchronously with the \SI{40}{\mega\hertz} clock used for readout.  This allows for the study of particles arriving at all times within the window.

A copy of the scintillator signal used for triggering the DUT is fed into the data stream of the Timepix3 telescope.  A shared time-stamp is used to match tracks reconstructed from the Timepix3 hits with the DUT data for the corresponding trigger.

To test the effects of radiation, sensors were irradiated at the IRRAD~\cite{irrad} facility using protons with a momentum of \SI{24}{\giga\eV}.  Dosimetry measurements were used to determine the particle fluence in the \SI{3}{\milli\meter} band surrounding the pitch adapter.  Sensors with six different fluences ranging from zero to \SI{2e13}{\neq} were used in our tests.  The latter fluence represents the maximum expected for UT sensors with these pitch adapters after 50\invfb of data collected in LHCb.

Miniature sensors from this study were found to have signal-to-noise ratios of at least 20.  Further information on similar beam tests conducted with prototype sensors from the same project may be found in Ref.~\cite{Abba:2137551}.  


\subsection{Data sample and analysis}

In order to isolate the effects of the pitch adapter and compare to simulation, specific beam events are chosen in which the passing particle goes through the bond pads of the pitch adapter.  Only events in which a single track is found in the Timepix3 telescope are selected.  Using the coordinates from tracks extrapolated to the DUT position, the side and top edges of the sensor are determined.  The position of the track in the local $x$ (perpendicular to each sensor strip) and local $y$ (parallel to each strip) directions is then calculated.
Results from multiple runs with the same sensor, taken in a short period of time, are combined together, but the position calibration is repeated for each run independently.

Fourteen strip pairs have been identified from the sensor design for which a single PA bond pad is found over the inter-strip region.  Some of these pads are centered between the two strips, and some are shifted towards one side or the other.  Tracks passing within \SI{19}{\micro\meter} of the center of the inter-strip region, at the correct range of local $y$ values to be under the bond pad are identified.  These 14 rectangular regions together make up the ``PA region''.  For each strip pair, the channel that is connected to the PA bond pad in that region is also identified.  The output for these three channels are then studied together to analyze the effect of the pitch adapter.  

For comparison, a control region is defined using the inter-strip regions of the same 14 strip pairs.  The control region requires the track to pass away from the PA region by requiring values of local $y$ farther down the sensor.  The signal response in the control region has been validated to be free of pitch adapter effects by comparing it with tracks that pass directly underneath a single strip.

For each event in the sample, the ADC counts for the two channels corresponding to the nearby strips as well as the channel connected to the PA bond pad are studied.  The ADC values used are taken after pedestal and common-mode noise subtraction are performed.  Events in which one of the three strips (nearby or PA connected) is flagged as noisy or dead are removed from the analysis.

The data output also includes a timing value for the event.  The \SI{25}{\nano\second} readout window is divided into ten \SI{2.5}{\nano\second} time bins, corresponding to different trigger arrival times relative to the \SI{40}{\mega\hertz} readout clock.  The data in each time bin is therefore sampled from the analog Beetle output pulse at different times relative to the start time of the pulse.  This allows us to reconstruct the shape of the output pulse inside the \SI{25}{\nano\second} window.
